import streamlit as st
import time
import numpy as np
import pandas as pd
import pyhvwrapper


hv = pyhvwrapper.HV('128.141.149.246', 3, 'admin', 'admin')
hv.connect()
crate_map = hv.crate_map
slot = 2
rpcs = [3, 2]

rpc_names = hv.get_ch_name(slot, rpcs)
vmons = hv.get_param(slot, rpcs, 'VMon')
v0sets = hv.get_param(slot, rpcs, 'V0Set')
imons = hv.get_param(slot, rpcs, 'IMon')
pws = hv.get_param(slot, rpcs, 'Pw')
statuses = hv.get_param(slot, rpcs, 'Status')
hv.disconnect()

cols = st.beta_columns(len(rpcs))
listeners = {}

for ix, col in enumerate(cols):
    channel = rpcs[ix]
    name = rpc_names[channel]
    vmon = vmons[channel]
    v0set = v0sets[channel]
    imon = imons[channel]
    pw = pws[channel]
    pw_string = 'On' if pw == True else 'Off'
    status = statuses[channel]
    with col:
        st.header(name)
        st.text(
            f'Channel : {channel},\nName: {name},\nV0Set: {v0set}\nVMon:{vmon},\nImon: {imon},\nPw: {pw}\nStatus: {status}')

        key = f'{channel}-toggle'
        a = st.button(label='Toggle', key=key)
        listeners[key] = a

if any(listeners.values()):
    st.text('Toggled')
    keys = [k for k, v in listeners.items() if v]
    # Start RPC
    for key in keys:
        hv.connect()
        channel = int(key.strip('-toggle'))
        current_pw = hv.get_param(slot, channel, 'Pw')[channel]
        res = hv.set_param(slot, channel, 'Pw', 1 - current_pw)
        hv.disconnect()
        st.text(f'Result: {res}')
        st.experimental_rerun()
